/* eslint-disable */
import { validatenull } from './validate'

const keyName = process.env.VUE_APP_KEY || 'xingyue'
const DOMAIN = process.env.VUE_APP_BASE_URL || '';
const MAXAGE = process.env.VUE_APP_MAXAGE || 1 * 24 * 60 * 60;
/**
 * 设置storage缓存
 * @param {String or Object} name  
 * @param {any} content  
 * @param {Boolean} type  
 */
export function setStore(params) {
    if (params.constructor === Object) var { name = null, content = null, type = false } = params;
    else var [name, content, type = null] = [...arguments]
    name = keyName + name
    let obj = {
        dataType: typeof (content),
        content: content,
        type: type,
        datetime: Date.now()
    }
    let str = btoa(encodeURIComponent(JSON.stringify(obj)));
    if (type) window.sessionStorage.setItem(name, str)
    else window.localStorage.setItem(name, str)
}

/**
 * 获取Storage
 */
export function getStore(params) {

    if (params.constructor === Object) var { name = null, debug = null, type = false } = params;
    else var [name, debug = null] = [...arguments]
    name = keyName + name;
    let obj = {}, content = null;
    obj = window.sessionStorage.getItem(name)
    if (validatenull(obj)) obj = window.localStorage.getItem(name)
    if (validatenull(obj)) return;
    obj = decodeURIComponent(atob(obj))
    try {
        obj = JSON.parse(obj)
    } catch (obj) {
        return obj
    }
    if (debug) {
        return obj
    }
    if (obj.dataType == 'string') {
        content = obj.content
    } else if (obj.dataType == 'number') {
        content = Number(obj.content)
    } else if (obj.dataType == 'boolean') {
        content = eval(obj.content)
    } else if (obj.dataType == 'object') {
        content = obj.content
    }

    return content
}
/**
 * 删除localStorage
 */
export function removeStore(params) {

    if (params.constructor === Object) var { name = null, type = null } = params;
    else var [name, type = null] = [...arguments];
    name = keyName + name;
    if (type == null) {
        window.sessionStorage.removeItem(name)
        window.localStorage.removeItem(name)
    } else if (type) {
        window.sessionStorage.removeItem(name)
    } else {
        window.localStorage.removeItem(name)
    }
}

/**
 * 获取全部localStorage
 */
export function getAllStore(params = {}) {
    let list = []
    let { type } = params
    if (type) {
        for (let i = 0; i <= window.sessionStorage.length; i++) {
            list.push({
                name: window.sessionStorage.key(i),
                content: getStore({
                    name: window.sessionStorage.key(i),
                    type: 'session'
                })
            })
        }
    } else {
        for (let i = 0; i <= window.localStorage.length; i++) {
            list.push({
                name: window.localStorage.key(i),
                content: getStore({
                    name: window.localStorage.key(i),
                })
            })

        }
    }
    return list
}

/**
 * 清空全部localStorage
 */
export function clearStore(params = false) {
    let { type } = params
    if (type) {
        window.sessionStorage.clear()
    } else {
        window.localStorage.clear()
    }

}


/**
 * 清空全部Storage
 */
export function clearStoreAll() {
    window.sessionStorage.clear()
    window.localStorage.clear()
}

/**
 * 设置cookie
 * @param {Object} params 包含name和data
 */
export function setCookie(params = {}) {
    try {
        if (params.constructor === Object) var { name = null, data = null } = params;
        else var [name, data = null] = [...arguments];
        name = keyName + name;
        data = btoa(encodeURIComponent(JSON.stringify({ content: data })));
        // data = JSON.stringify({ content: data })
        document.cookie = `${name}=${data}; Max-Age=${MAXAGE}; domain=${DOMAIN};path=/`
        return true
    } catch (error) {
        return false
    }
}
/**
 * 获取cookie
 */
export function getCookie(name) {
    name = keyName + name;
    let cookie = document.cookie;
    let cookies = {};
    cookie.split('; ').map(e => e.split('=')).forEach(e => {
        cookies[e[0]] = e[1]
    })
    cookie = cookies[name] ? decodeURIComponent(atob(cookies[name])) : false;
    // cookie = cookies[name] ? window.atob(cookies[name]) : false;
    if (cookie) {
        return JSON.parse(cookie).content
    } else {
        return false
    }
}
/**
 * 删除cookie
 */
export function clearCookie(name) {
    name = keyName + name;
    document.cookie = `${name}=0; Max-Age=0; domain=${DOMAIN};path=/`
}
export default { removeStore, getAllStore, getStore, clearStore, setStore, setCookie, getCookie, clearCookie }